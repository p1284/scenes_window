using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gameready.EditorTools.ScenesWindow;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Gameready.EditorTools.UIToolkit.ScenesWindow
{
    public class ScenesWindow : EditorWindow
    {
        private const string EditorResourceFolder = "Editor Default Resources";

        private List<EditorBuildSettingsScene> _scenes;

        private ScenesWindowCache _cache;

        [SerializeField] private VisualTreeAsset _warnLabel;
        [SerializeField] private VisualTreeAsset _folderFoldout;
        [SerializeField] private VisualTreeAsset _sceneButtonBarAsset;
        [SerializeField] private VisualTreeAsset _uxmlAsset;

        private string _sceneSearchMask;
        private Foldout _gameScenesFold;
        private Foldout _favoriteFold;
        private Foldout _others;

        [MenuItem("Window/Scenes", false, -1999)]
        public static void ShowUIToolkitScenesWindow()
        {
            ScenesWindow wnd = GetWindow<ScenesWindow>();
            wnd.titleContent = new GUIContent("Scenes");
            try
            {
                wnd.Show();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                wnd.Close();
            }
        }

        private void CreateNewCacheAsset()
        {
            var path = $"{Application.dataPath}/{EditorResourceFolder}";
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            var assetPath = $"Assets/{EditorResourceFolder}/ScenesWindowCacheAsset.asset";
            AssetDatabase.CreateAsset(_cache, assetPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private void SaveCacheData()
        {
            if (_cache != null)
            {
                EditorUtility.SetDirty(_cache);
                AssetDatabase.SaveAssets();
            }
        }

        private string[] PrepareScenes()
        {
            _cache = _cache != null
                ? _cache
                : EditorGUIUtility.Load("ScenesWindowCacheAsset.asset") as ScenesWindowCache;

            if (_cache == null)
            {
                _cache = ScriptableObject.CreateInstance<ScenesWindowCache>();
                _cache.scenes = new List<SceneCacheData>();
                CreateNewCacheAsset();
            }

            var guids = AssetDatabase.FindAssets("t:Scene");
            var paths = Array.ConvertAll<string, string>(guids, AssetDatabase.GUIDToAssetPath);

            if (_scenes != null)
            {
                if (_scenes.Count != EditorBuildSettings.scenes.Length)
                {
                    _scenes = EditorBuildSettings.scenes.ToList();
                }
                else
                {
                    for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
                    {
                        _scenes[i] = new EditorBuildSettingsScene(EditorBuildSettings.scenes[i].guid,
                            EditorBuildSettings.scenes[i].enabled);
                    }
                }

                AssetDatabase.SaveAssets();
            }
            else
            {
                _scenes = EditorBuildSettings.scenes.ToList();
            }


            return paths;
        }

        private void FillScenes()
        {
            var paths = PrepareScenes();

            _cache.scenes.Clear();

            foreach (var path in paths)
            {
                var folder = Path.GetDirectoryName(path);
                var data = _cache.scenes.Find(cacheData => cacheData.sceneFolder == folder);

                if (data == null)
                {
                    data = new SceneCacheData { sceneFolder = folder, scenesPath = new List<string>() };
                    _cache.scenes.Add(data);
                }

                data.scenesPath.Add(path);
            }
        }

        private void OnEnable()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChangedHandler;
            EditorBuildSettings.sceneListChanged += OnSceneListChangedHandler;
        }

        private void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChangedHandler;
            EditorBuildSettings.sceneListChanged -= OnSceneListChangedHandler;
        }

        private void OnPlayModeStateChangedHandler(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.EnteredEditMode)
            {
                CreateGUI();
            }
        }

        private void OnSceneListChangedHandler()
        {
            RefreshAndSave();
        }

        private void RefreshAndSave()
        {
            if (Application.isPlaying) return;
            FillScenes();
            BuildGameScenes();
            BuildFavorites();
            BuildOtherScenes();
        }

        private void AddCurrentToBuild()
        {
            var scenes = EditorBuildSettings.scenes;
            var scene = SceneManager.GetActiveScene();

            foreach (var settingsScene in scenes)
            {
                if (settingsScene.path == scene.path)
                {
                    settingsScene.enabled = true;
                    _scenes = scenes.ToList();
                    EditorBuildSettings.scenes = _scenes.ToArray();
                    BuildGameScenes();
                    Debug.LogWarning("Scene already added to Build Scenes");
                    return;
                }
            }

            Array.Resize(ref scenes, scenes.Length + 1);
            scenes[^1] = new EditorBuildSettingsScene(scene.path, true);
            _scenes = scenes.ToList();

            EditorBuildSettings.scenes = _scenes.ToArray();
        }

        private void RemoveCurrentFromBuild()
        {
            var scene = SceneManager.GetActiveScene();

            foreach (var settingsScene in _scenes)
            {
                if (settingsScene.path == scene.path)
                {
                    _scenes.Remove(settingsScene);
                    break;
                }
            }

            EditorBuildSettings.scenes = _scenes.ToArray();
        }

        private void ReloadWindow()
        {
            Close();
            ShowUIToolkitScenesWindow();
        }

        public void CreateGUI()
        {
            _cache = EditorGUIUtility.Load("ScenesWindowCacheAsset.asset") as ScenesWindowCache;

            // Each editor window contains a root VisualElement object
            VisualElement root = rootVisualElement;
            root.Clear();

            if (Application.isPlaying)
            {
                VisualElement uxml = _warnLabel.Instantiate();
                root.Add(uxml);

                return;
            }

            if (_sceneButtonBarAsset == null)
            {
                throw new Exception("Scenes Window error: Setup button-bar visual tree asset");
            }


            if (_uxmlAsset != null)
            {
                // Import UXML
                VisualElement uxml = _uxmlAsset.Instantiate();
                root.Add(uxml);
            }

            var reloadButton = root.Q<Button>("reload");
            reloadButton.clicked -= ReloadWindow;
            reloadButton.clicked += ReloadWindow;

            var refreshSave = root.Q<Button>("refresh-save");
            refreshSave.clicked -= RefreshAndSave;
            refreshSave.clicked += RefreshAndSave;

            var addCurrentToBuild = root.Q<Button>("add-current-tobuild");
            addCurrentToBuild.clicked -= AddCurrentToBuild;
            addCurrentToBuild.clicked += AddCurrentToBuild;

            var removeCurrentFromBuild = root.Q<Button>("remove-current-frombuild");
            removeCurrentFromBuild.clicked -= RemoveCurrentFromBuild;
            removeCurrentFromBuild.clicked += RemoveCurrentFromBuild;

            var addCurrentToFavorite = root.Q<Button>("add-current-tofavorite");
            addCurrentToFavorite.clicked -= AddCurrentToFavorite;
            addCurrentToFavorite.clicked += AddCurrentToFavorite;

            var searchField = root.Q<ToolbarSearchField>("searchfield");
            searchField.UnregisterValueChangedCallback(OnSearchChangedCallback);
            searchField.RegisterValueChangedCallback(OnSearchChangedCallback);

            _gameScenesFold = root.Q<Foldout>("gamescenes");

            _favoriteFold = root.Q<Foldout>("favorites");

            _others = root.Q<Foldout>("others");

            FillScenes();

            BuildGameScenes();

            BuildFavorites();

            BuildOtherScenes();
        }

        private void BuildGameScenes()
        {
            _gameScenesFold.Clear();
            if (_scenes != null && _scenes.Count > 0)
            {
                //create game scenes
                foreach (var scene in _scenes)
                {
                    _gameScenesFold.Add(CreateGameScene(scene.path, scene.enabled));
                }
            }
        }

        private void BuildFavorites()
        {
            _favoriteFold.Clear();

            foreach (var favoriteScene in _cache.favoriteScenesData)
            {
                _favoriteFold.Add(CreateFavoriteScene(favoriteScene));
            }

            _favoriteFold.value = _favoriteFold.childCount > 0;
        }


        private void BuildOtherScenes()
        {
            _others.Clear();

            foreach (var data in _cache.scenes)
            {
                if (string.IsNullOrEmpty(_sceneSearchMask) || data.ContainScene(_sceneSearchMask))
                {
                    var foldout = _folderFoldout.Instantiate();
                    foldout.name = data.sceneFolder;
                    var folder = foldout.contentContainer.Q<Foldout>();
                    folder.text = data.sceneFolder;
                    folder.userData = data;
                    folder.value = data.foldOut;
                    folder.UnregisterValueChangedCallback(OnFolderChangedCallback);
                    folder.RegisterValueChangedCallback(OnFolderChangedCallback);
                    _others.Add(foldout);

                    foreach (var path in data.scenesPath)
                    {
                        if (_scenes.Count > 0 && _scenes.Exists(scene => scene.path == path)) continue;

                        folder.Add(CreateGameScene(path));
                    }
                }
            }
        }

        private void OnFolderChangedCallback(ChangeEvent<bool> evt)
        {
            if (evt.target is Foldout folderFoldout)
            {
                var sceneCacheData = folderFoldout.userData as SceneCacheData;
                sceneCacheData.foldOut = evt.newValue;
            }
        }

        private void RemoveFromFavorite(VisualElement favoriteBar, string sceneName)
        {
            if (_cache.favoriteScenesData.Contains(sceneName))
            {
                _cache.favoriteScenesData.Remove(sceneName);
                _favoriteFold.value = _cache.favoriteScenesData.Count > 0;
                _favoriteFold.Remove(favoriteBar);
                _favoriteFold.value = _favoriteFold.childCount > 0;
                SaveCacheData();
            }
        }

        private void AddCurrentToFavorite()
        {
            var scene = SceneManager.GetActiveScene();
            AddToFavorite(scene.path);
        }

        private void AddToFavorite(string sceneName)
        {
            if (!_cache.favoriteScenesData.Contains(sceneName))
            {
                _cache.favoriteScenesData.Add(sceneName);
                _favoriteFold.Add(CreateFavoriteScene(sceneName));
                _favoriteFold.value = _favoriteFold.childCount > 0;
                SaveCacheData();
            }
        }

        private bool OpenScene(string scenePath)
        {
            try
            {
                if (SceneManager.GetActiveScene().isDirty)
                {
                    var option =
                        EditorUtility.DisplayDialogComplex("Save scenes", "Save current scene?", "yes", "no", "cancel");

                    if (option == 0)
                    {
                        EditorSceneManager.SaveOpenScenes();
                    }
                }

                EditorSceneManager.OpenScene(scenePath);

                // var obj = AssetDatabase.LoadMainAssetAtPath(scenePath);
                // if (obj != null)
                // {
                //     Selection.activeObject = obj;
                //     EditorGUIUtility.PingObject(obj);
                // }

                return true;
            }
            catch (Exception e)
            {
                Debug.LogError("Scene cannot open! " + e.Message);
                RefreshAndSave();
            }

            return false;
        }

        private VisualElement CreateFavoriteScene(string scenePath)
        {
            var bar = _sceneButtonBarAsset.Instantiate();
            bar.name = scenePath;

            var sceneButton = bar.Q<Button>("scene-button");
            sceneButton.text = Path.GetFileName(scenePath);
            sceneButton.RegisterCallback<MouseUpEvent>(evt =>
            {
                if (!OpenScene(scenePath))
                {
                    if (_cache.favoriteScenesData.Contains(scenePath))
                    {
                        _cache.favoriteScenesData.Remove(scenePath);
                    }
                }
            });

            var iconButton = bar.Q<Button>("icon-button");
            iconButton.Add(new Image()
            {
                image = EditorGUIUtility.IconContent("d_TreeEditor.Trash").image
            });

            iconButton.RegisterCallback<MouseUpEvent>(evt => { RemoveFromFavorite(bar, scenePath); });

            return bar;
        }

        private VisualElement CreateGameScene(string scenePath, bool enabled = true)
        {
            var bar = _sceneButtonBarAsset.Instantiate();
            bar.name = scenePath;

            var sceneButton = bar.Q<Button>("scene-button");
            sceneButton.text = Path.GetFileName(scenePath);
            sceneButton.userData = scenePath;
            sceneButton.SetEnabled(enabled);
            sceneButton.UnregisterCallback<MouseUpEvent>(_ => OpenScene(scenePath));
            sceneButton.RegisterCallback<MouseUpEvent>(evt => OpenScene(scenePath));

            var iconButton = bar.Q<Button>("icon-button");
            iconButton.Add(new Image()
            {
                image = EditorGUIUtility.IconContent("d_Favorite").image
            });

            iconButton.RegisterCallback<MouseUpEvent>(evt => AddToFavorite(scenePath));

            return bar;
        }

        private void OnSearchChangedCallback(ChangeEvent<string> evt)
        {
            _sceneSearchMask = evt.newValue;
            _others.value = true;
            BuildOtherScenes();
        }
    }
}